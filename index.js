import { AppRegistry } from 'react-native'
import React from 'react'
import App from './src/app'
import { name as appName } from './app.json'
import { Provider } from 'react-redux'
import { configureStore } from './src/redux/configure-store'

const store = configureStore({})
const AppWithRedux = () => (
	<Provider store={store}>
		<App />
	</Provider>
)

AppRegistry.registerComponent(appName, () => AppWithRedux)
