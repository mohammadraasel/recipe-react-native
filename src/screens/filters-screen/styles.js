import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
	screen: {
		flex: 1,
		alignItems: 'center',
		backgroundColor: '#eee',
	},
	title: {
		fontSize: 20,
		margin: 20,
		fontFamily: 'open-sans-bold',
		fontWeight: 'bold',
		textAlign: 'center',
	},
	filterContainer: {
		width: '80%',
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		marginVertical: 10,
	},
	saveButtonContainer: {
		marginTop: 20,
	},
	saveButton: {
		backgroundColor: 'orange',
		paddingHorizontal: 14,
		paddingVertical: 8,
		elevation: 2,
	},
	buttonLebel: {
		fontSize: 18,
	},
})
