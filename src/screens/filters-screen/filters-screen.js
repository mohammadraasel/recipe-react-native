import React from 'react'
import { Text, View, Switch } from 'react-native'
import { styles } from './styles'
import { useSelector, useDispatch } from 'react-redux'
import { setFilters } from '../../redux/recipe/actions'
const FilterSwitch = props => {
	return (
		<View style={styles.filterContainer}>
			<Text>{props.lebel}</Text>
			<Switch
				trackColor={{ true: 'orange' }}
				thumbColor="white"
				value={props.value}
				onValueChange={props.onValueChange}
			/>
		</View>
	)
}

const FiltersScreen = ({ navigation }) => {
	const { filterSettings } = useSelector(state => state.recipeStates)
	const dispatch = useDispatch()

	const onFiltersChange = (value, optionName) => {
		dispatch(setFilters(value, optionName))
	}

	return (
		<View style={styles.screen}>
			<Text style={styles.title}>Available Filters / Restrictions</Text>
			<FilterSwitch
				lebel="Gluten Free"
				value={filterSettings.glutenFree}
				onValueChange={value => onFiltersChange(value, 'glutenFree')}
			/>
			<FilterSwitch
				lebel="Lactose Free"
				value={filterSettings.lactoseFree}
				onValueChange={value => onFiltersChange(value, 'lactoseFree')}
			/>
			<FilterSwitch
				lebel="Vegan"
				value={filterSettings.vegan}
				onValueChange={value => onFiltersChange(value, 'vegan')}
			/>
			<FilterSwitch
				lebel="Vegeterian"
				value={filterSettings.vegeterian}
				onValueChange={value => onFiltersChange(value, 'vegetarian')}
			/>
		</View>
	)
}

export default FiltersScreen
