import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
	screenContainer: {
		flex: 1,
	},
	indicatorContainer: {
		flex: 1,
		justifyContent: 'center',
	},
})
