import React, { useEffect } from 'react'
import { Routes } from '../../constants/routes'
import RecipeList from '../../components/recipe-list'
import { getRecipes } from '../../redux/recipe/actions'
import { useDispatch, useSelector } from 'react-redux'
import LoadingSpinner from '../../components/loading-spinner'
import FallbackText from '../../components/fallback-text'

const RecipesScreen = ({ navigation, route }) => {
	const categoryId = route.params.id
	const dispatch = useDispatch()
	const { isFetchingRecipes, filteredRecipes } = useSelector(
		state => state.recipeStates,
	)

	const onRecipeSelected = recipe => {
		navigation.navigate(Routes.RECIPE_DETAIL, {
			id: recipe.id,
			title: recipe.title,
		})
	}

	useEffect(() => {
		dispatch(getRecipes(categoryId))
	}, [categoryId, dispatch])

	if (isFetchingRecipes) {
		return <LoadingSpinner color="black" size="large" />
	} else if (filteredRecipes.length !== 0 && !isFetchingRecipes) {
		return (
			<RecipeList
				recipes={filteredRecipes}
				onRecipeSelected={onRecipeSelected}
			/>
		)
	} else {
		return <FallbackText text="No recipes" />
	}
}

export default RecipesScreen
