import React from 'react'
import RecipeList from '../../components/recipe-list'
import { Routes } from '../../constants/routes'
import { useSelector } from 'react-redux'
import FallbackText from '../../components/fallback-text'

const FavoritesScreen = ({ navigation }) => {
	const favRecipes = useSelector(state => state.recipeStates.favoriteRecipes)

	const onRecipeSelected = recipe => {
		navigation.navigate(Routes.RECIPE_DETAIL, {
			id: recipe.id,
			title: recipe.title,
		})
	}

	if (!favRecipes || favRecipes.length === 0) {
		return <FallbackText text="No fovorite recipes" />
	}
	return (
		<RecipeList onRecipeSelected={onRecipeSelected} recipes={favRecipes} />
	)
}

export default FavoritesScreen
