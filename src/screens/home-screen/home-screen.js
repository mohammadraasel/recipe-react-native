import React from 'react'
import { Text, Button } from 'react-native'
import Center from '../../components/center'

const HomeScreen = ({ navigation }) => {
	return (
		<Center>
			<Text>Home Screen</Text>
			<Button
				title="Go to Categories"
				onPress={() =>
					navigation.navigate('Categories', {
						itemId: 86,
						otherParam: 'anything you want here',
					})
				}
			/>
		</Center>
	)
}

export default HomeScreen
