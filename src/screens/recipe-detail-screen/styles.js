import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
	image: {
		height: 200,
	},
	container: {},
	row: {
		height: 30,
		flexDirection: 'row',
		justifyContent: 'space-between',
		paddingHorizontal: 20,
		alignItems: 'center',
		backgroundColor: 'rgba(255, 165, 0, 0.1)',
		marginBottom: 10,
	},
	rowText: {
		color: 'orange',
		fontWeight: 'bold',
		fontSize: 15,
	},
	title: {
		fontSize: 18,
		fontWeight: 'bold',
		fontFamily: 'open-sans-bold',
		textAlign: 'center',
	},
	listItem: {
		borderColor: '#ccc',
		borderWidth: 1,
		marginHorizontal: 20,
		marginVertical: 8,
		padding: 10,
	},
})
