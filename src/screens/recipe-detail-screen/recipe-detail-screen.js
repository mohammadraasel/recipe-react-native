import React, { useLayoutEffect, useEffect, useCallback } from 'react'
import { Text, View, ScrollView, Image } from 'react-native'
import FavoriteButton from '../../components/favorite-button'
import { styles } from './styles'
import { capitalize } from '../../utils'
import { useSelector, useDispatch } from 'react-redux'
import { getRecipe, toggleFavoriteRecipe } from '../../redux/recipe/actions'
import LoadingSpinner from '../../components/loading-spinner'

const RecipeDetailScreen = ({ navigation, route }) => {
	const recipeId = route.params.id
	const { isFetchingRecipe, recipeDetail } = useSelector(
		state => state.recipeStates,
	)
	const isFavorite = useSelector(state => {
		return !!state.recipeStates.favoriteRecipes.find(r => r.id === recipeId)
	})
	const dispatch = useDispatch()

	const toggleFavorite = useCallback(() => {
		dispatch(toggleFavoriteRecipe(recipeId))
	}, [recipeId, dispatch])

	useEffect(() => {
		dispatch(getRecipe(recipeId))
	}, [recipeId, dispatch])

	useLayoutEffect(() => {
		navigation.setOptions({
			headerRight: () => (
				<FavoriteButton
					isFavorite={isFavorite}
					toggleFavorite={toggleFavorite}
				/>
			),
		})
	}, [navigation, isFavorite, toggleFavorite])

	if (recipeDetail && !isFetchingRecipe) {
		return (
			<ScrollView style={styles.container}>
				<Image
					source={{ uri: recipeDetail.imageUrl }}
					style={styles.image}
				/>
				<View style={styles.row}>
					<Text style={styles.rowText}>
						{recipeDetail.duration} min
					</Text>
					<Text style={styles.rowText}>
						{capitalize(recipeDetail.complexity)}
					</Text>
					<Text style={styles.rowText}>
						{capitalize(recipeDetail.affordability)}
					</Text>
				</View>
				<Text style={styles.title}>Ingredients</Text>
				{recipeDetail.ingredients.map(ingred => (
					<View style={styles.listItem} key={ingred}>
						<Text style={styles.listText}>{ingred}</Text>
					</View>
				))}
				<Text style={styles.title}>Steps</Text>
				{recipeDetail.steps.map(step => (
					<View style={styles.listItem} key={step}>
						<Text style={styles.listText}>{step}</Text>
					</View>
				))}
			</ScrollView>
		)
	} else {
		return <LoadingSpinner />
	}
}

export default RecipeDetailScreen
