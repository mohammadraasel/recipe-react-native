import React, { useEffect } from 'react'
import { FlatList } from 'react-native'
import { Routes } from '../../constants/routes'
import CategoryGridTile from '../../components/category-grid-tile/category-grid-tile'
import { useSelector, useDispatch } from 'react-redux'
import { getCategories } from '../../redux/category/actions'
import LoadingSpinner from '../../components/loading-spinner'

const CategoriesScreen = ({ navigation }) => {
	const dispatch = useDispatch()
	const { isFetchingCategories, categories } = useSelector(
		state => state.categoryStates,
	)

	const onSelected = categoryItem => {
		navigation.navigate(Routes.RECIPES, {
			id: categoryItem.id,
			title: categoryItem.title,
		})
	}
	useEffect(() => {
		dispatch(getCategories())
	}, [dispatch])

	if (isFetchingCategories) {
		return <LoadingSpinner />
	} else {
		return (
			<FlatList
				data={categories}
				renderItem={({ item }) => (
					<CategoryGridTile category={item} onSelected={onSelected} />
				)}
				numColumns={2}
			/>
		)
	}
}

export default CategoriesScreen
