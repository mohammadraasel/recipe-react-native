import { DefaultTheme } from '@react-navigation/native'

export const theme = {
	...DefaultTheme,
	colors: {
		...DefaultTheme.colors,
		primary: 'black',
		background: '#fff',
		card: '#FFD500',
		text: 'black',
		border: '#7D7D80',
	},
}
