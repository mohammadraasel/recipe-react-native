import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import FiltersScreen from '../../screens/filters-screen'
import MenuButton from '../../components/menu-button'

const FilterStack = createStackNavigator()
const FilterStackNavigator = ({ navigation }) => {
	return (
		<FilterStack.Navigator>
			<FilterStack.Screen
				name="Filters"
				component={FiltersScreen}
				options={{
					headerLeft: () => <MenuButton navigation={navigation} />,
				}}
			/>
		</FilterStack.Navigator>
	)
}

export default FilterStackNavigator
