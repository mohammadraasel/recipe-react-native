import React from 'react'
import { createStackNavigator } from '@react-navigation/stack'
import FavoritesScreen from '../../screens/favorites-screen'
import RecipeDetailScreen from '../../screens/recipe-detail-screen'
import { Routes } from '../../constants/routes'
import MenuButton from '../../components/menu-button'

const FavoriteStack = createStackNavigator()

const FavoriteStackNavigator = ({ navigation }) => (
	<FavoriteStack.Navigator
		initialRouteName={Routes.FAVORITES}
		screenOptions={{ headerTitleAlign: 'center' }}>
		<FavoriteStack.Screen
			name={Routes.FAVORITES}
			component={FavoritesScreen}
			options={{
				headerLeft: () => <MenuButton navigation={navigation} />,
			}}
		/>
		<FavoriteStack.Screen
			options={({ route }) => ({
				title: route.params.title,
				headerTitleStyle: {
					fontSize: 17,
				},
			})}
			name={Routes.RECIPE_DETAIL}
			component={RecipeDetailScreen}
		/>
	</FavoriteStack.Navigator>
)

export default FavoriteStackNavigator
