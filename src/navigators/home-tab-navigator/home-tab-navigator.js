import React from 'react'
import Icon from 'react-native-vector-icons/FontAwesome5'
import CategoryStackNavigator from '../category-stack-navigator'
import FavoriteStackNavigator from '../favorite-stack-navigator'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'

const Tab = createBottomTabNavigator()

const HomeTabNavigator = () => {
	const getTabScreensOptions = ({ route }) => ({
		tabBarIcon: ({ color, size }) => {
			let iconName
			if (route.name === 'Categories') {
				iconName = 'list'
			} else if (route.name === 'Favorites') {
				iconName = 'star'
			}

			return <Icon size={size} color={color} name={iconName} />
		},
	})

	const tabBarOptions = {
		activeTintColor: 'orange',
		inactiveTintColor: 'black',
		activeBackgroundColor: 'white',
		inactiveBackgroundColor: 'white',
	}
	return (
		<Tab.Navigator
			tabBarOptions={tabBarOptions}
			screenOptions={getTabScreensOptions}>
			<Tab.Screen name="Categories" component={CategoryStackNavigator} />
			<Tab.Screen name="Favorites" component={FavoriteStackNavigator} />
		</Tab.Navigator>
	)
}

export default HomeTabNavigator
