import React from 'react'
import CategoriesScreen from '../../screens/categories-screen'
import RecipesScreen from '../../screens/recipes-screen'
import RecipeDetailScreen from '../../screens/recipe-detail-screen'
import { Routes } from '../../constants/routes'
import { createStackNavigator } from '@react-navigation/stack'
import MenuButton from '../../components/menu-button'

const CategoryStack = createStackNavigator()

const CategoryNavigator = ({ navigation }) => {
	return (
		<CategoryStack.Navigator
			initialRouteName={Routes.CATEGORIES}
			screenOptions={{ headerTitleAlign: 'center' }}>
			<CategoryStack.Screen
				options={{
					headerTitle: 'Recipe Category',
					headerLeft: () => <MenuButton navigation={navigation} />,
				}}
				name={Routes.CATEGORIES}
				component={CategoriesScreen}
			/>
			<CategoryStack.Screen
				options={({ route }) => ({
					title: route.params.title,
				})}
				name={Routes.RECIPES}
				component={RecipesScreen}
			/>
			<CategoryStack.Screen
				options={({ route }) => ({
					title: route.params.title,
					headerTitleStyle: {
						fontSize: 17,
					},
				})}
				name={Routes.RECIPE_DETAIL}
				component={RecipeDetailScreen}
			/>
		</CategoryStack.Navigator>
	)
}

export default CategoryNavigator
