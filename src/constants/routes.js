export const Routes = {
	CATEGORIES: 'Categories',
	RECIPES: 'Recipes',
	RECIPE_DETAIL: 'Detail',
	FAVORITES: 'Favorites',
}
