export const Colors = {
	primary: '#FFD400',
	seconday: '#FEA301',
	primary1: '#F6CD56',
	accent: '#F4F4F4',
	accent1: '#272624',
	greyish: '#7D7D80',
	text: '#BCB6A5',
	error: '#E53D07',
}
