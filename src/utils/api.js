import { RECIPES, CATEGORIES } from '../data/fakeData'

export const API = {}

API.fetchRecipesByCategoryId = categoryId => {
	const recipes = RECIPES.filter(recipe =>
		recipe.categoryIds.includes(categoryId),
	) // actually data will be fetched from db

	return new Promise((resolve, reject) => {
		if (recipes) {
			setTimeout(() => {
				resolve(recipes)
			}, 500)
		} else {
			reject({ message: 'something went wrong!' })
		}
	})
}

API.fetchRecipeById = recipeId => {
	const selectedRecipe = RECIPES.find(recipe => recipe.id === recipeId) // actually data will be fetched from db

	return new Promise((resolve, reject) => {
		if (selectedRecipe) {
			setTimeout(() => {
				resolve(selectedRecipe)
			}, 500)
		} else {
			reject({ message: 'something went wrong!' })
		}
	})
}

API.fetchCategories = () => {
	const categories = [...CATEGORIES] // actually data will be fetched from db
	return new Promise((resolve, reject) => {
		if (categories) {
			setTimeout(() => {
				resolve(categories)
			}, 500)
		} else {
			reject({ message: 'something went wrong!' })
		}
	})
}
