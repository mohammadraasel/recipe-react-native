import React from 'react'
import { StyleSheet, View, ActivityIndicator } from 'react-native'

const LoadingSpinner = props => {
	return (
		<View style={styles.indicatorContainer}>
			<ActivityIndicator
				size={props.size ? props.size : 'large'}
				color={props.color ? props.color : 'black'}
			/>
		</View>
	)
}

export default LoadingSpinner

const styles = StyleSheet.create({
	indicatorContainer: {
		flex: 1,
		justifyContent: 'center',
	},
})
