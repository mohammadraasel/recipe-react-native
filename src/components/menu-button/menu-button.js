import React from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'

const MenuButton = props => {
	return (
		<TouchableOpacity
			activeOpacity={0.5}
			style={styles.menu}
			onPress={() => {
				props.navigation.toggleDrawer()
			}}>
			<Icon name="bars" size={20} color="#000" />
		</TouchableOpacity>
	)
}

export default MenuButton

const styles = StyleSheet.create({
	menu: {
		marginLeft: 10,
	},
})
