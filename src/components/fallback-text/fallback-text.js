import React from 'react'
import { StyleSheet, Text, View } from 'react-native'

const FallBackText = props => {
	return (
		<View style={styles.container}>
			<Text style={styles.text}>{props.text}</Text>
		</View>
	)
}

export default FallBackText

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
	text: {
		fontSize: 15,
		color: '#0984e3',
	},
})
