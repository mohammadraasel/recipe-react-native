import React from 'react'
import { Text, View, TouchableOpacity, ImageBackground } from 'react-native'
import { styles } from './styles'
import { capitalize } from '../../utils'

const RecipeItem = ({ recipe, onSelected }) => {
	return (
		<View style={styles.recipeContainer}>
			<TouchableOpacity
				style={styles.touchableArea}
				activeOpacity={0.8}
				onPress={() => onSelected(recipe)}>
				<View>
					<View
						style={{ ...styles.recipeRow, ...styles.recipeHeader }}>
						<ImageBackground
							style={styles.bgImage}
							source={{ uri: recipe.imageUrl }}>
							<Text style={styles.title}>{recipe.title}</Text>
						</ImageBackground>
					</View>
					<View style={{ ...styles.recipeRow, ...styles.recipeInfo }}>
						<Text>{recipe.duration}min</Text>
						<Text>{capitalize(recipe.complexity)}</Text>
						<Text>{capitalize(recipe.affordability)}</Text>
					</View>
				</View>
			</TouchableOpacity>
		</View>
	)
}

export default RecipeItem
