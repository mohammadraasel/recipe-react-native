import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
	recipeContainer: {
		height: 200,
		margin: 10,
	},
	touchableArea: {
		backgroundColor: '#ccc',
		flex: 1,
	},
	recipeRow: {
		flexDirection: 'row',
	},
	recipeHeader: {
		height: '88%',
	},
	title: {
		fontFamily: 'open-sans-bold',
		fontWeight: 'bold',
		fontSize: 17,
		color: 'white',
		textAlign: 'center',
		paddingHorizontal: 12,
		paddingVertical: 5,
		backgroundColor: 'rgba(0, 0, 0, .35)',
	},
	recipeInfo: {
		paddingHorizontal: 10,
		justifyContent: 'space-between',
		alignItems: 'center',
		height: '12%',
		backgroundColor: 'gold',
	},
	bgImage: {
		width: '100%',
		height: '100%',
		justifyContent: 'flex-start',
	},
})
