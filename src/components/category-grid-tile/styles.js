import { StyleSheet } from 'react-native'

export const styles = StyleSheet.create({
	gridItem: {
		flex: 1,
		margin: 12,
		height: 150,
		borderRadius: 10,
		elevation: 3,
	},
	container: {
		padding: 0,
		flex: 1,
		borderRadius: 10,
		justifyContent: 'center',
		alignItems: 'center',
		shadowColor: 'black',
		shadowOpacity: 0.26,
		shadowRadius: 10,
		shadowOffset: { height: 2, width: 0 },
	},
	title: {
		fontFamily: 'open-sans-bold',
		fontSize: 16,
		padding: 5,
		fontWeight: 'bold',
	},
})
