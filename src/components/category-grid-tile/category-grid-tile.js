import React from 'react'
import { Text, View, TouchableOpacity } from 'react-native'
import { styles } from './styles'
const CategoryGridTile = ({ category, onSelected }) => {
	return (
		<TouchableOpacity
			activeOpacity={0.8}
			style={styles.gridItem}
			onPress={() => onSelected(category)}>
			<View
				style={{
					...styles.container,
					backgroundColor: category.color,
				}}>
				<Text style={styles.title} numberOfLines={2}>
					{category.title}
				</Text>
			</View>
		</TouchableOpacity>
	)
}

export default CategoryGridTile
