import React from 'react'
import { View, FlatList } from 'react-native'
import RecipeItem from '../recipe-item'
import { styles } from './styles'

const RecipeList = props => {
	return (
		<View style={styles.screenContainer}>
			<FlatList
				data={props.recipes}
				renderItem={({ item }) => (
					<RecipeItem
						recipe={item}
						onSelected={props.onRecipeSelected}
					/>
				)}
			/>
		</View>
	)
}

export default RecipeList
