import React from 'react'
import { TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome5'
import { Colors } from '../../constants/colors'
import { styles } from './styles'

const FavoriteButton = ({ isFavorite, toggleFavorite }) => {
	return (
		<TouchableOpacity
			style={styles.favoriteButton}
			activeOpacity={0.6}
			onPress={() => {
				toggleFavorite()
			}}>
			<Icon
				name="star"
				size={20}
				color={Colors.accent1}
				solid={isFavorite}
			/>
		</TouchableOpacity>
	)
}

export default FavoriteButton
