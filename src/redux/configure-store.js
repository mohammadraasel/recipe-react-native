import { createStore } from 'redux'
import { rootReducer } from './root-reducer'
import { middlewares } from './middlewares'

export const configureStore = preloadedState => {
	const store = createStore(rootReducer, preloadedState, middlewares)
	return store
}
