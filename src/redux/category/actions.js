import {
	FETCH_CATEGORIES_REQUEST,
	FETCH_CATEGORIES_SUCCESS,
	FETCH_CATEGORIES_FAILURE,
} from './action-types'
import { API } from '../../utils/api'

export const getCategories = () => dispatch => {
	dispatch({ type: FETCH_CATEGORIES_REQUEST })
	return API.fetchCategories()
		.then(response => {
			dispatch({ type: FETCH_CATEGORIES_SUCCESS, payload: response })
		})
		.catch(err => {
			dispatch({ type: FETCH_CATEGORIES_FAILURE })
			console.log(err)
		})
}
