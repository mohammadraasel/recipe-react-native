import {
	FETCH_CATEGORIES_REQUEST,
	FETCH_CATEGORIES_SUCCESS,
	FETCH_CATEGORIES_FAILURE,
} from './action-types'

const initialState = {
	isFetchingCategories: false,
	categories: [],
}

export const categories = (state = initialState, { type, payload }) => {
	switch (type) {
		case FETCH_CATEGORIES_REQUEST:
			return {
				...state,
				isFetchingCategories: true,
			}
		case FETCH_CATEGORIES_SUCCESS:
			return {
				...state,
				isFetchingCategories: false,
				categories: [...payload],
			}
		case FETCH_CATEGORIES_FAILURE:
			return {
				...state,
				isFetchingCategories: false,
			}
		default:
			return state
	}
}
