import { combineReducers } from 'redux'
import { recipes } from './recipe/reducer'
import { categories } from './category/reducer'

export const rootReducer = combineReducers({
	recipeStates: recipes,
	categoryStates: categories,
})
