import { applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'

const middlewareList = [thunk]

export const middlewares = compose(applyMiddleware(...middlewareList))
