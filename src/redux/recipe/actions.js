import {
	FETCH_RECIPES_FAILURE,
	FETCH_RECIPES_REQUEST,
	FETCH_RECIPES_SUCCESS,
	FETCH_RECIPE_REQUEST,
	FETCH_RECIPE_SUCCESS,
	FETCH_RECIPE_FAILURE,
	TOGGLE_FAVORITE_RECIPE,
	SET_FILTERS,
} from './action-types'
import { API } from '../../utils/api'

export const getRecipes = id => dispatch => {
	dispatch({ type: FETCH_RECIPES_REQUEST })
	return API.fetchRecipesByCategoryId(id)
		.then(response => {
			dispatch({ type: FETCH_RECIPES_SUCCESS, payload: response })
		})
		.catch(err => {
			dispatch({ type: FETCH_RECIPES_FAILURE })
			console.log(err)
		})
}

export const getRecipe = id => dispatch => {
	dispatch({ type: FETCH_RECIPE_REQUEST })
	return API.fetchRecipeById(id)
		.then(response => {
			dispatch({ type: FETCH_RECIPE_SUCCESS, payload: response })
		})
		.catch(err => {
			dispatch({ type: FETCH_RECIPE_FAILURE })
			console.log(err)
		})
}

export const toggleFavoriteRecipe = recipeId => dispatch => {
	dispatch({ type: TOGGLE_FAVORITE_RECIPE, payload: { recipeId } })
}

export const setFilters = (value, optionName) => dispatch => {
	dispatch({ type: SET_FILTERS, payload: { value, optionName } })
}
