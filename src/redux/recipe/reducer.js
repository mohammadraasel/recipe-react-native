import { RECIPES } from '../../data/fakeData'
import {
	FETCH_RECIPES_SUCCESS,
	FETCH_RECIPES_REQUEST,
	FETCH_RECIPES_FAILURE,
	FETCH_RECIPE_REQUEST,
	FETCH_RECIPE_SUCCESS,
	FETCH_RECIPE_FAILURE,
	TOGGLE_FAVORITE_RECIPE,
	SET_FILTERS,
} from './action-types'

const initialState = {
	recipes: RECIPES,
	isFetchingRecipes: false,
	filteredRecipes: [],
	favoriteRecipes: [],
	isFetchingRecipe: false,
	recipeDetail: null,
	filterSettings: {
		glutenFree: false,
		lactoseFree: false,
		vegan: false,
		vegetarian: false,
	},
}

export const recipes = (state = initialState, { type, payload }) => {
	switch (type) {
		case FETCH_RECIPES_REQUEST:
			return {
				...state,
				isFetchingRecipes: true,
			}
		case FETCH_RECIPES_SUCCESS:
			return {
				...state,
				isFetchingRecipes: false,
				filteredRecipes: applyFilters(state.filterSettings, payload),
			}
		case FETCH_RECIPES_FAILURE:
			return {
				...state,
				isFetchingRecipes: false,
			}
		case FETCH_RECIPE_REQUEST:
			return {
				...state,
				isFetchingRecipe: true,
			}
		case FETCH_RECIPE_SUCCESS:
			return {
				...state,
				isFetchingRecipe: false,
				recipeDetail: { ...payload },
			}
		case FETCH_RECIPE_FAILURE:
			return {
				...state,
				isFetchingRecipe: false,
			}
		case TOGGLE_FAVORITE_RECIPE:
			return toggleFavorite(state, payload.recipeId)
		case SET_FILTERS:
			return {
				...state,
				filterSettings: {
					...state.filterSettings,
					[payload.optionName]: payload.value,
				},
			}
		default:
			return state
	}
}

const toggleFavorite = (state, recipeId) => {
	const currentRecipe = state.recipes.find(recipe => recipe.id === recipeId)
	if (currentRecipe) {
		const isExist = state.favoriteRecipes.find(
			recipe => recipe.id === recipeId,
		)
		let newFavoriteRecipes = []
		if (isExist) {
			newFavoriteRecipes = state.favoriteRecipes.filter(
				recipe => recipe.id !== recipeId,
			)
		} else {
			newFavoriteRecipes = [...state.favoriteRecipes, currentRecipe]
		}
		return {
			...state,
			favoriteRecipes: newFavoriteRecipes,
		}
	} else {
		return state
	}
}

const applyFilters = (filterOptions, payload) => {
	if (Object.keys(filterOptions).some(option => filterOptions[option])) {
		return payload.filter(recipe => {
			if (filterOptions.glutenFree && !recipe.isGlutenFree) {
				return false
			}
			if (filterOptions.lactoseFree && !recipe.isLactoseFree) {
				return false
			}
			if (filterOptions.vegan && !recipe.isVegan) {
				return false
			}
			if (filterOptions.vegetarian && !recipe.isVegetarian) {
				return false
			}
			return true
		})
	}
	return [...payload]
}
