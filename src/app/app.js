import React from 'react'
import { StyleSheet } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'
import { createDrawerNavigator } from '@react-navigation/drawer'
import { theme } from '../theme/theme'
import HomeTabNavigator from '../navigators/home-tab-navigator'
import FilterStackNavigator from '../navigators/filter-stack-navigator'

const Drawer = createDrawerNavigator()

const App = () => {
	return (
		<NavigationContainer theme={theme}>
			<Drawer.Navigator
				drawerContentOptions={{
					activeTintColor: 'orange',
					labelStyle: {
						fontFamily: 'open-sans-bold',
						fontWeight: 'bold',
					},
				}}
				drawerStyle={styles.drawerStyle}>
				<Drawer.Screen
					options={{ drawerLabel: 'Recipes' }}
					name="Home"
					component={HomeTabNavigator}
				/>
				<Drawer.Screen
					name="Filters"
					component={FilterStackNavigator}
				/>
			</Drawer.Navigator>
		</NavigationContainer>
	)
}

export default App

const styles = StyleSheet.create({
	drawerStyle: {
		backgroundColor: '#fff',
	},
})
